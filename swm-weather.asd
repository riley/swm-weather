(asdf:defsystem #:swm-weather
  :serial t
  :author "Riley"
  :version "0.1.0"
  :license "CC0"
  :depends-on (:stumpwm :cl-ppcre :dexador :s-xml)
  :components ((:file "src/package")
               (:file "src/swm-weather")))
